<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Animal;

class Report extends Model
{
    protected $table = 'reports';
	
    protected $fillable = ['total_animal', 'total_dead', 'summary', 'date'];
}

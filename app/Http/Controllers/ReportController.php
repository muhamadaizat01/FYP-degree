<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Animal;
use App\Report;
use Carbon\Carbon;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = array();
        $reports['January'] = null;
        $reports['February'] = null;
        $reports['March'] = null;
        $reports['April'] = null;
        $reports['May'] = null;
        $reports['June'] = null;
        $reports['July'] = null;
        $reports['August'] = null;
        $reports['September'] = null;
        $reports['October'] = null;
        $reports['November'] = null;
        $reports['December'] = null;

        // dd($reports);
        $report_data = Report::all()->toArray();

        foreach ($report_data as $data) {
            $date = date_create($data['date']);
            $date_in_month = date_format($date, "F");
            $date_in_month; // return August

            $reports[$date_in_month] = $data;
        }

        // dd($reports);

        return view('form.adminviewreport', compact('reports'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     $animal_count = Animal::
       whereMonth('date_of_arrival', Carbon::now()->month)
     ->whereYear('date_of_arrival', Carbon::now()->year)
     ->count();

    $dead_animal_count = Animal::
       whereMonth('date_of_death', Carbon::now()->month)
     ->whereYear('date_of_death', Carbon::now()->year)
     ->count();

        return view('form.adminreport', compact('animal_count', 'dead_animal_count'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $report = Report::create([
            'total_animal' => $request->total_animal,
            'total_dead' => $request->total_dead,
            'summary' => $request->summary,
            'date' => Carbon::createFromFormat('Y-m', $request->date),
        ]);
        return view('/adminhome');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $report = Report::find($id);
// dd($report->date);
        $converted_date = Carbon::createFromFormat('Y-m-d', $report->date);
        $date_in_month = $converted_date->month; 
        $date_in_year = $converted_date->year;
// dd($converted_date);
        $animal_count = Animal::
       whereMonth('date_of_arrival', $date_in_month)
     ->whereYear('date_of_arrival', $date_in_year)
     ->count();
     // dd($animal_count);
    $dead_animal_count = Animal::
       whereMonth('date_of_death', $date_in_month)
     ->whereYear('date_of_death', $date_in_year)
     ->count();

        return view('form.admineditreport', compact('report', 'id', 'animal_count', 'dead_animal_count'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $report = Report::find($id);
        $report->total_animal = $request->get('total_animal');
        $report->total_dead = $request->get('total_dead');
        $report->summary = $request->get('summary');
        $report->save();
        return redirect('form/adminviewreport');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

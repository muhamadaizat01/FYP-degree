<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\AdminController;
use App\Animal;

class AdminEditController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $animal = Animal::findorFail($id);
        return view('form.adminviewanimal2', compact('animal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $animal = Animal::find($id);
        return view('form.admineditanimal', compact('animal', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'unique_id' => 'unique:animal,unique_id,' .$id,
        ]);
        
        $animal = Animal::find($id);
        $animal->name_of_species = $request->get('name_of_species');
        $animal->scientific_name = $request->get('scientific_name');
        $animal->unique_id = $request->get('unique_id');
        $animal->date_of_birth = $request->get('date_of_birth');
        $animal->animal_category = $request->get('animal_category');
        $animal->animal_sex = $request->get('animal_sex');
        $animal->animal_habitat = $request->get('animal_habitat');
        $animal->date_of_arrival = $request->get('date_of_arrival');
        $animal->time_of_arrival = $request->get('time_of_arrival');
        $animal->date_of_death = $request->get('date_of_death');
        $animal->save();
        return redirect('form/adminviewanimal')->with('success', 'Data Updated');
    }

    public function updateDeath(Request $request, $id)
    {
        $animal = Animal::find($id);
        $animal->date_of_death = $request->get('date_of_death');
        $animal->save();
        return redirect('form/adminviewanimal')->with('success', 'Date of Death Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

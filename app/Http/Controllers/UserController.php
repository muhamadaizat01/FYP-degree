<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\AdminController;
use Illuminate\Support\Facades\Input;
use App\User;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all()->toArray();
        return view('form.adminviewstaff', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findorFail($id);
        return view('form.viewprofile', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('form.editprofile', compact('user', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'email' => 'unique:users,email,' .$id,
            'phone_number' => 'unique:users,phone_number,' . $id,
        ]);

        $user = User::find($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->firstname = $request->get('firstname');
        $user->lastname = $request->get('lastname');
        $user->gender = $request->get('gender');
        $user->phone_number = $request->get('phone_number');
        $user->address = $request->get('address');
        $user->save();
        return redirect()->to('form/viewprofile/'. $id)->with('success', 'Profile Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->back()->with('success', 'Data Deleted');
    }

    public function search()
    {
        $search = Input::get('search');
        if($search != ''){
            $user = User::where('name', 'LIKE', '%'.$search.'%')
                            ->orWhere('email', 'LIKE', '%'.$search.'%')
                            ->paginate(5)
                            ->setpath('');
            $user->appends(array(
                $search => Input::get('search')
            ));
        }
        else {
            $user = User::paginate(20);            
        }
        return view('form.adminviewstaff', compact('user'))->withData($user);
    }

    public function staffprofile($id)
    {
        $user = User::findorFail($id);
        return view('form.adminviewstaff2', compact('user'));
    }
}

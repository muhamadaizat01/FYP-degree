<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\AdminController;
use Illuminate\Support\Facades\Input;
use App\Animal;

class FormController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     // compact = creates an array containing variables and their values
    
    public function index()
    {
        $animal = Animal::all()->toArray();
        if (Auth()->check() && Auth()->user()->isAdmin()) {
        return view('form.adminviewanimal', compact('animal'));
       }
       else {
        return view('form.viewanimal', compact('animal'));
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth()->check() && Auth()->user()->isAdmin()) {
        return view('form.adminaddanimal');
       }
       else {
        return view('form.addanimal');
       }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'unique_id' => 'unique:animal'
        ]);

        $animal = Animal::create([
            'name_of_species' => $request->name_of_species,
            'scientific_name' => $request->scientific_name,
            'unique_id' => $request->unique_id,
            'animal_category' => $request->animal_category,
            'animal_sex' => $request->animal_sex,
            'animal_habitat' => $request->animal_habitat,
            'date_of_arrival' => $request->date_of_arrival,
            'date_of_birth' => $request->date_of_birth,
            'time_of_arrival' => $request->time_of_arrival,
            'date_of_death' => $request->date_of_death,
            'last_edited_by' => auth()->user()->name,
        ]);
        if (Auth()->check() && Auth()->user()->isAdmin()) {
        return redirect('/form/adminviewanimal')->with('success', 'New Animal Added');
       }
       else {
        return redirect('/form/viewanimal')->with('success', 'New Animal Added');
       }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $animal = Animal::findorFail($id);
        return view('form.viewanimal2', compact('animal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $animal = Animal::find($id);
        return view('form.editanimal', compact('animal', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'unique_id' => 'unique:animal,unique_id,' .$id,
        ]);
        
        $animal = Animal::find($id);
        $animal->name_of_species = $request->get('name_of_species');
        $animal->scientific_name = $request->get('scientific_name');
        $animal->unique_id = $request->get('unique_id');
        $animal->date_of_birth = $request->get('date_of_birth');
        $animal->animal_category = $request->get('animal_category');
        $animal->animal_sex = $request->get('animal_sex');
        $animal->animal_habitat = $request->get('animal_habitat');
        $animal->date_of_arrival = $request->get('date_of_arrival');
        $animal->time_of_arrival = $request->get('time_of_arrival');
        $animal->date_of_death = $request->get('date_of_death');
        $animal->last_edited_by = auth()->user()->name;
        $animal->save();
        return redirect('form/viewanimal')->with('success', 'Data Updated');
    }

    public function updateDeath(Request $request, $id)
    {
        $animal = Animal::find($id);
        $animal->date_of_death = $request->get('date_of_death');
        $animal->save();
        return redirect('form/viewanimal')->with('success', 'Date of Death Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $animal = Animal::find($id);
        $animal->delete();
        return redirect()->back()->with('success', 'Data Deleted'); 
    }

    public function search()
    {
        $search = Input::get('search');
        if($search != ''){
            $animal = Animal::where('scientific_name', 'LIKE', '%'.$search.'%')
                            ->orWhere('unique_id', 'LIKE', '%'.$search.'%')
                            ->paginate(5)
                            ->setpath('');
            $animal->appends(array(
                $search => Input::get('search')
            ));
        }
        else {
            $animal = Animal::paginate(20);            
        }

        if (Auth()->check() && Auth()->user()->isAdmin()) 
        {
        return view('form.adminviewanimal', compact('animal'))->withData($animal);
       }
       else {
        return view('form.viewanimal', compact('animal'))->withData($animal);
       }
    }
}

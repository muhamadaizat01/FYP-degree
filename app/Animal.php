<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
	protected $table = 'animal';
	
    protected $fillable = ['name_of_species','scientific_name','unique_id','animal_category','animal_sex','animal_habitat','date_of_arrival','date_of_birth','time_of_arrival','date_of_death','last_edited_by'];
}

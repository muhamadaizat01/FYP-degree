<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/adminhome', 'AdminController@admin')->middleware('is_admin')->name('admin');

Route::get('/form/addanimal', 'FormController@create');
Route::get('/form/adminaddanimal', 'FormController@create');

Route::post('/form/addanimal', 'FormController@store');
Route::post('/form/adminaddanimal', 'FormController@store');

Route::get('/form/viewanimal', 'FormController@index');
Route::get('/form/adminviewanimal', 'FormController@index');

Route::delete('/form/adminviewanimal/{id}', 'FormController@destroy');

Route::get('form.editanimal/{id}', 'FormController@edit');
Route::put('form.viewanimal/{id}', 'FormController@update');
Route::put('/form/animaldead/{animal_id}', 'FormController@updateDeath')->name('dead_animal');

Route::get('form.admineditanimal/{id}', 'AdminEditController@edit');
Route::put('form.adminviewanimal/{id}', 'AdminEditController@update');
Route::put('/form/admin/animaldead/{animal_id}', 'AdminEditController@updateDeath')->name('admin.dead_animal');

Route::get('form/adminviewstaff', 'UserController@index');
Route::delete('form/adminviewstaff/{id}', 'UserController@destroy');

Route::get('/form/viewprofile/{id}', 'UserController@show');
Route::get('form.editprofile/{id}', 'UserController@edit');
Route::put('form.viewprofile/{id}', 'UserController@update');

Route::get('register', 'Auth\UserRegisterController@showRegistrationForm')->name('register'); 
Route::post('register', 'Auth\UserRegisterController@register');

Route::any('/form/search', 'FormController@search'); 
Route::any('/form/search2', 'UserController@search');

Route::get('/form/viewanimal2/{id}', 'FormController@show');
Route::get('/form/adminviewanimal2/{id}', 'AdminEditController@show');

Route::get('/form/adminviewstaff2/{id}', 'UserController@staffprofile');

Route::get('/form/adminreport', 'ReportController@create');
Route::post('/form/adminreport', 'ReportController@store');

Route::get('form/adminviewreport','ReportController@index');

Route::get('form.admineditreport/{id}', 'ReportController@edit');
Route::put('form.adminviewreport/{id}', 'ReportController@update');
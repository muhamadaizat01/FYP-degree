<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'System Admin (Aizat)',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('password'),
            'type' => 'admin lalala'
        ]);
    }
}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-2 cs-padding-0">
            @include('layouts.horizontalnav')
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Profile Form</div>
                <div class="card-body">
                    <h5 class="card-title">Edit Your Profile Details Here</h5>
                
                @if(count($errors) > 0)
                <div class="alert alert danger">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form action="{{action('UserController@update', $id)}}" method="post" onsubmit="return confirm('Do you really want to update the profile?');">
                
                {{ csrf_field() }}
                {{ method_field('PUT')}}
                <input type="hidden" name="_method" value="PUT"/>
                <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{$user->name}}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{$user->email}}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="firstname" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>

                            <div class="col-md-6">
                                <input id="firstname" type="text" class="form-control" name="firstname" value="{{$user->firstname}}" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text" class="form-control" name="lastname" value="{{$user->lastname}}" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

                            <div class="col-md-6">
                            <input class="form-control" id="gender" name="gender" type="text" value="{{$user->gender}}" readonly/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone_number" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                            <div class="col-md-6">
                                <input id="phone_number" type="text" class="form-control" name="phone_number" pattern="[01][0-9]{9}" value="{{$user->phone_number}}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                            <div class="col-md-6">
                                <textarea id="address" class="form-control" name="address" rows="4" required>{{$user->address}}</textarea>
                            </div>
                        </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-info float-right" value="Update">
                </div>
         
            </div>
                </div>       
                    </div>
                    
                </form>
                <div class="col-md-2"></div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>

@endsection
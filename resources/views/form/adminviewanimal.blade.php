@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<div class="container">
<div class="row">
	<div class="col-md-2 cs-padding-0">
            @include('layouts.adminhorizontalnav')
        </div>
	<div class="col-md-10">
		<br/>
		<h3 align="center">Animal Data</h3>
		@if (count($animal) == 0)
			No record found!
		@else
			<div class="col-lg-4">
			<form action="/form/search" method="post" role="search">
				{{ csrf_field() }}
    		<div class="input-group">
      		<input type="text" class="form-control" name="search" placeholder="Scientific Name or Unique ID only">
      			<button type="submit" class="btn btn-default" aria-label="Left Align">
  				<span class="fa fa-search" aria-hidden="true"></span>
				</button> 
    		</div><!-- /input-group -->
  			</div>
		<br/>
		@if($message = Session::get('success'))
		<div class="alert alert-success alert-dismissible fade show" role="alert">
			<span>{{$message}}</span>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    <span aria-hidden="true">&times;</span>
		    </button>
		</div>
		@endif
		<br/>
		<table class="table table-striped table-bordered">
			<thead class="thead-dark">
				<tr>
					<th>Name of Species</th>
					<th>Scientific Name</th>
					<th>Unique ID</th>
					<th>Last Edited By</th>
					<th style="text-align: center">Input Date of Death</th>
					<th style="text-align: center">View</th>
					<th style="text-align: center">Edit</th>
					<th style="text-align: center">Delete</th>
				</tr>
				<tbody>
				@foreach($animal as $animal)
					<tr>
						<td>{{$animal['name_of_species']}}</td>
						<td>{{$animal['scientific_name']}}</td>
						<td>{{$animal['unique_id']}}</td>
						<td>{{$animal['last_edited_by']}}</td>

					<td class="text-center"><a class="btn btn-info fa fa-heartbeat" data-toggle="modal" data-target="#exampleModal{{$loop->iteration}}"></a>
					@include('form.modal')
					</td>
					<td class="text-center"><a href="{{action('AdminEditController@show', $animal['id'])}}"><i class="btn btn-light fa fa-eye"></i></a>
					</td>
					<td class="text-center"><a href="{{action('AdminEditController@edit', $animal['id'])}}" class="btn btn-warning fa fa-pencil"></a>
					</td>
					<td class="text-center"><form method="post" class="delete_form" action="{{action('FormController@destroy', [$animal['id']])}}">
					{{ csrf_field() }}
					<input type="hidden" name="_method" value="DELETE" />
					<button type="submit" class="btn btn-danger fa fa-times"></button> 	
					</form>
					</td> 
					</tr>
				@endforeach
				</tbody>
			</thead>
		</table>
		@endif
	</div>
</div>
</div>
<script>
	$(document).ready(function(){
		$('.delete_form').on('submit', function(){
			if(confirm("Are you sure you want to delete it?"))
			{
				return true;
			}
			else
			{
				return false;
			}
		});
	});
</script>	
@endsection
@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<div class="container">
<div class="row">
	<div class="col-md-2 cs-padding-0">
            @include('layouts.adminhorizontalnav')
        </div>
	<div class="col-md-10">
		<br/>
		<h3 align="center">Staff Data</h3>
		@if (count($user) == 0)
			No record found!
		@else
			<div class="col-lg-3">
			<form action="/form/search2" method="post" role="search">
				{{ csrf_field() }}
    		<div class="input-group">
      		<input type="text" class="form-control" name="search" placeholder="Name or Email only">
      			<button type="submit" class="btn btn-default" aria-label="Left Align">
  				<span class="fa fa-search" aria-hidden="true"></span>
				</button> 
    		</div><!-- /input-group -->
			</form>
  			</div>
		<br/>
		@if($message = Session::get('success'))
        	<div class="alert alert-success alert-dismissible fade show" role="alert">
        		<span>{{$message}}</span>
        		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        		<span aria-hidden="true">&times;</span>
        		</button>
        	</div>
        @endif
		<br/>
		<table class="table table-striped table-bordered">
			<thead class="thead-dark">
				<tr>
					<th>Username</th>
					<th>Email</th>
					<th style="text-align: center">View</th>
					<th style="text-align: center">Delete</th>
				</tr>
				<tbody>
				@foreach($user as $user)
					<tr>
						<td>{{$user['name']}}</td>
						<td>{{$user['email']}}</td>

					<td style="text-align: center"><a href="{{action('UserController@staffprofile', $user['id'])}}" class="btn btn-light fa fa-eye"></a>
					</td>
					<td style="text-align: center">
						<form method="post" class="delete_form" action="{{action('UserController@destroy', [$user['id']])}}">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="DELETE" />
						<button type="submit" class="btn btn-danger fa fa-times"></button> 	
					</form>
					</td> 
					</tr>
				@endforeach
				</tbody>
			</thead>
		</table>
		@endif
	</div>
</div>
</div>
<script>
	$(document).ready(function(){
		$('.delete_form').on('submit', function(){
			if(confirm("Are you sure you want to delete this profile?"))
			{
				return true;
			}
			else
			{
				return false;
			}
		});
	});
</script>	
@endsection
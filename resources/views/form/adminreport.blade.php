@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-2 cs-padding-0">
            @include('layouts.adminhorizontalnav')
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Your Monthly Report for {{date('F Y')}}</div>
            <div class="card-body">
                <form action="/form/adminreport" onsubmit="myFunction()" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                <label for="total_animal">Current Total Animal:</label>
                <div class="col-sm-10">
                <p><input type="hidden" id="total_animal" name="total_animal" value="{{$animal_count}}" class="form-control-static">{{$animal_count}}</p>
                </div>
                </div>
                
                <div class="form-group">
                <label for="total_dead">Total Animal Died:</label>
                <div class="col-sm-10">
                <p><input type="hidden" id="total_dead" name="total_dead" value="{{$dead_animal_count}}" class="form-control-static">{{$dead_animal_count}}</p>
                </div>
                </div>

                <div class="form-group">
                <label for="summary">Summary of the report:</label>
                <textarea class="form-control" id="summary" name="summary" rows="4" required></textarea>
                </div>

                <input type="hidden" name="date" value="{{date('Y-m')}}">

                <div class="form-group">
                    <input type="submit" class="btn btn-info" value="Submit">
                    <input type="reset" class="btn btn-info" value="Reset">
                </div>

                    </div>
                </form>
                </div>
            </div>
            <div class="col-md-2"></div>
            </div>
        </div>
    </div>
</div>

<script>
    function myFunction() {
    alert("Animal Summary Report Submitted");
    }
</script>
@endsection
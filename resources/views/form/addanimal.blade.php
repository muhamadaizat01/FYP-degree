@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-2 cs-padding-0">
            @include('layouts.horizontalnav')
        </div>
        <div class="col-md-8">
            
            {{-- error display --}}
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{-- error display --}}

            <div class="card">
                <div class="card-header">New Animal Form</div>
            <div class="card-body">
                <h5 class="card-title">Add Animal Entry Here</h5>
                <form action="/form/addanimal"  autocomplete="off" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                <label for="name">Name of Species:</label>
                <input type="text" class="form-control" id="name" name="name_of_species" placeholder="e.g Elephant" required>
                </div>
                
                <div class="form-group">
                <label for="scientific_name">Scientific Name:</label>
                <input type="text" class="form-control" id="scientific_name" name="scientific_name" placeholder="e.g Martis Kagiro" required>
                </div>

                <div class="form-group">
                <label for="unique_id">Unique ID:</label>
                <input type="text" class="form-control" id="unique_id" name="unique_id" placeholder="e.g ABC123" required>
                </div>

                <div class="form-group"> <!-- Date input -->
                    <label class="control-label" for="date">Date of Birth</label>
                    <input class="form-control" id="date" name="date_of_birth" type="date" required/>
                </div>

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                           <label for="animal_category">Animal Category:</label>
                                <select class="form-control" name="animal_category" id="animal_category" required>
                                    <option value="">Please choose</option>
                                    <option value="Amphibians">Amphibians</option>
                                    <option value="Bird">Bird</option>
                                    <option value="Fish">Fish</option>
                                    <option value="Mammals">Mammals</option>
                                    <option value="Reptiles">Reptiles</option>
                                </select>
                        </div>
                        <div class="col-md-6">
                            <label for="animal_sex">Animal Sex:</label>
                                <select class="form-control" id="animal_sex" name="animal_sex" required>
                                    <option value="">Please choose</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-12">
                            <label for="habitat">Animal Habitat:</label>
                                <input type="text" class="form-control" id="animal_habitat" name="animal_habitat" list="suggestions" placeholder="e.g Lion Habitat" required>
                                    <datalist id="suggestions">
                                        <option value="Giraffe Habitat">
                                        <option value="Elephant Habitat">
                                        <option value="Lion Habitat">
                                        <option value="Gorilla Habitat">
                                        <option value="Hipopotamus Habitat">
                                        <option value="Cheetah Habitat">
                                        <option value="Deer Habitat">
                                        <option value="Buffalo Habitat">
                                        <option value="Polar Habitat">
                                        <option value="Chimpanzee Habitat">
                                        <option value="Tiger Habitat">
                                        <option value="Bird Habitat">
                                        <option value="Crocodile Habitat">
                                        <option value="Snake Habitat">
                                        <option value="Panda Habitat">
                                        <option value="Aquarium Habitat">
                                    </datalist>
                            </div>
                        </div>
                    </div>

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6"> <!-- Date input -->
                            <label class="control-label" for="date">Date of Arrival</label>
                            <input class="form-control" id="date_of_arrival" name="date_of_arrival" type="date" required/>
                        </div>

                        <div class="col-md-6"> <!-- Time input -->
                            <label class="control-label" for="time">Time of Arrival</label>
                            <input class="form-control" id="time_of_arrival" name="time_of_arrival" type="time" required/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-info" value="Submit">
                    <input type="reset" class="btn btn-info" value="Reset">
                </div>

                    </div>
                </form>
                </div>
            </div>
            <div class="col-md-2"></div>
            </div>
        </div>
    </div>
</div>

{{-- <script>
    function myFunction() {
    alert("Animal Added");
    }
</script> --}}

<script type="text/javascript">
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
        if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        } 
    today = yyyy+'-'+mm+'-'+dd;
    document.getElementById("date").setAttribute("max", today);
</script>

@endsection
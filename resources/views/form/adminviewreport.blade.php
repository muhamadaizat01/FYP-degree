@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
    <div class="col-md-2 cs-padding-0">
            @include('layouts.adminhorizontalnav')
    </div>
    <div class ="col-md-8">
    <div class="card-deck">
    <div class="card bg-warning" style="{{ $reports['January'] == null ? 'opacity: 0.5;' : '' }}">
      <div class="card-body text-center">
        <h5 class="card-title">January</h5>
        <hr>
        <p class="card-text">
        Total animal: {{ $reports['January']['total_animal']}} <br>
        Total dead: {{ $reports['January']['total_dead']}}
        </p>

        {{-- toggle collapsible --}}
          <a class="btn btn-primary" data-toggle="collapse" href="#January" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Summary</a>

            <div class="collapse multi-collapse" id="January">
              <div class="card card-body">
                {{ $reports['January']['summary']}}  
              </div>
            </div>
        {{-- toggle collapsible --}}

        @if ($reports['January'] == null)
        <a class="btn btn-primary" href="#">Edit</a>
        @else
        <a class="btn btn-primary" href="{{action('ReportController@edit',$reports['January']['id'])}}">Edit</a>
        @endif

      </div>

    </div>
    <div class="card bg-warning" style="{{ $reports['February'] == null ? 'opacity: 0.5;' : '' }}">
      <div class="card-body text-center">
        <h5 class="card-title">February</h5>
        <hr>
        <p class="card-text">
        Total animal: {{ $reports['February']['total_animal']}} <br>
        Total dead: {{ $reports['February']['total_dead']}}
        </p>

        <a class="btn btn-primary" data-toggle="collapse" href="#February" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Summary</a>

            <div class="collapse multi-collapse" id="February">
              <div class="card card-body">
                {{ $reports['February']['summary']}}  
              </div>
            </div>

            @if ($reports['February'] == null)
            <a class="btn btn-primary" href="#">Edit</a>
            @else
            <a class="btn btn-primary" href="{{action('ReportController@edit',$reports['February']['id'])}}">Edit</a>
            @endif

      </div>
    </div>
    <div class="card bg-warning" style="{{ $reports['March'] == null ? 'opacity: 0.5;' : '' }}">
      <div class="card-body text-center">
        <h5 class="card-title">March</h5>
        <hr>
        <p class="card-text">
        Total animal: {{ $reports['March']['total_animal']}} <br>
        Total dead: {{ $reports['March']['total_dead']}}
        </p>

        <a class="btn btn-primary" data-toggle="collapse" href="#March" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Summary</a>

            <div class="collapse multi-collapse" id="March">
              <div class="card card-body">
                {{ $reports['March']['summary']}}  
              </div>
            </div>

            @if ($reports['March'] == null)
            <a class="btn btn-primary" href="#">Edit</a>
            @else
            <a class="btn btn-primary" href="{{action('ReportController@edit',$reports['March']['id'])}}">Edit</a>
            @endif

      </div>
    </div>  
  </div>
<br/>
<div class="card-deck">
    <div class="card bg-warning" style="{{ $reports['April'] == null ? 'opacity: 0.5;' : '' }}">
      <div class="card-body text-center">
        <h5 class="card-title">April</h5>
        <hr>
        <p class="card-text">
        Total animal: {{ $reports['April']['total_animal']}} <br>
        Total dead: {{ $reports['April']['total_dead']}}
        </p>

        <a class="btn btn-primary" data-toggle="collapse" href="#April" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Summary</a>

            <div class="collapse multi-collapse" id="April">
              <div class="card card-body">
                {{ $reports['April']['summary']}}  
              </div>
            </div>

            @if ($reports['April'] == null)
            <a class="btn btn-primary" href="#">Edit</a>
            @else
            <a class="btn btn-primary" href="{{action('ReportController@edit',$reports['April']['id'])}}">Edit</a>
            @endif

      </div>
    </div>
    <div class="card bg-warning" style="{{ $reports['May'] == null ? 'opacity: 0.5;' : '' }}">
      <div class="card-body text-center">
        <h5 class="card-title">May</h5>
        <hr>
        <p class="card-text">
        Total animal: {{ $reports['May']['total_animal']}} <br>
        Total dead: {{ $reports['May']['total_dead']}}
        </p>

        <a class="btn btn-primary" data-toggle="collapse" href="#May" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Summary</a>

            <div class="collapse multi-collapse" id="May">
              <div class="card card-body">
                {{ $reports['May']['summary']}}  
              </div>
            </div>

            @if ($reports['May'] == null)
            <a class="btn btn-primary" href="#">Edit</a>
            @else
            <a class="btn btn-primary" href="{{action('ReportController@edit',$reports['May']['id'])}}">Edit</a>
            @endif

      </div>
    </div>
    <div class="card bg-warning" style="{{ $reports['June'] == null ? 'opacity: 0.5;' : '' }}">
      <div class="card-body text-center">
        <h5 class="card-title">June</h5>
        <hr>
        <p class="card-text">
        Total animal: {{ $reports['June']['total_animal']}} <br>
        Total dead: {{ $reports['June']['total_dead']}}
        </p>

        <a class="btn btn-primary" data-toggle="collapse" href="#June" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Summary</a>

            <div class="collapse multi-collapse" id="June">
              <div class="card card-body">
                {{ $reports['June']['summary']}}  
              </div>
            </div>

            @if ($reports['June'] == null)
            <a class="btn btn-primary" href="#">Edit</a>
            @else
            <a class="btn btn-primary" href="{{action('ReportController@edit',$reports['June']['id'])}}">Edit</a>
            @endif

      </div>
    </div>
</div>
<br/>
<div class="card-deck">
    <div class="card bg-warning" style="{{ $reports['July'] == null ? 'opacity: 0.5;' : '' }}">
      <div class="card-body text-center">
        <h5 class="card-title">July</h5>
        <hr>
        <p class="card-text">
        Total animal: {{ $reports['July']['total_animal']}} <br>
        Total dead: {{ $reports['July']['total_dead']}}
        </p>

        <a class="btn btn-primary" data-toggle="collapse" href="#July" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Summary</a>

            <div class="collapse multi-collapse" id="July">
              <div class="card card-body">
                {{ $reports['July']['summary']}}  
              </div>
            </div>

            @if ($reports['July'] == null)
            <a class="btn btn-primary" href="#">Edit</a>
            @else
            <a class="btn btn-primary" href="{{action('ReportController@edit',$reports['July']['id'])}}">Edit</a>
            @endif

      </div>
    </div>  
    <div class="card bg-warning" style="{{ $reports['August'] == null ? 'opacity: 0.5;' : '' }}">
      <div class="card-body text-center">
        <h5 class="card-title">August</h5>
        <hr>
        <p class="card-text">
        Total animal: {{ $reports['August']['total_animal']}} <br>
        Total dead: {{ $reports['August']['total_dead']}}
        </p>

        <a class="btn btn-primary" data-toggle="collapse" href="#August" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Summary</a>

            <div class="collapse multi-collapse" id="August">
              <div class="card card-body">
                {{ $reports['August']['summary']}}  
              </div>
            </div>

            @if ($reports['August'] == null)
            <a class="btn btn-primary" href="#">Edit</a>
            @else
            <a class="btn btn-primary" href="{{action('ReportController@edit',$reports['August']['id'])}}">Edit</a>
            @endif

      </div>
    </div>
    <div class="card bg-warning" style="{{ $reports['September'] == null ? 'opacity: 0.5;' : '' }}">
      <div class="card-body text-center">
        <h5 class="card-title">September</h5>
        <hr>
        <p class="card-text">
        Total animal: {{ $reports['September']['total_animal']}} <br>
        Total dead: {{ $reports['September']['total_dead']}}
        </p>

        <a class="btn btn-primary" data-toggle="collapse" href="#September" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Summary</a>

            <div class="collapse multi-collapse" id="September">
              <div class="card card-body">
                {{ $reports['September']['summary']}}  
              </div>
            </div>

            @if ($reports['September'] == null)
            <a class="btn btn-primary" href="#">Edit</a>
            @else
            <a class="btn btn-primary" href="{{action('ReportController@edit',$reports['September']['id'])}}">Edit</a>
            @endif

      </div>
    </div>
</div>
<br>
<div class="card-deck">
    <div class="card bg-warning" style="{{ $reports['October'] == null ? 'opacity: 0.5;' : '' }}">
      <div class="card-body text-center">
        <h5 class="card-title">October</h5>
        <hr>
        <p class="card-text">
        Total animal: {{ $reports['October']['total_animal']}} <br>
        Total dead: {{ $reports['October']['total_dead']}}
        </p>

        <a class="btn btn-primary" data-toggle="collapse" href="#October" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Summary</a>

            <div class="collapse multi-collapse" id="October">
              <div class="card card-body">
                {{ $reports['October']['summary']}}  
              </div>
            </div>

            @if ($reports['October'] == null)
            <a class="btn btn-primary" href="#">Edit</a>
            @else
            <a class="btn btn-primary" href="{{action('ReportController@edit',$reports['October']['id'])}}">Edit</a>
            @endif

      </div>
    </div>  
    <div class="card bg-warning" style="{{ $reports['November'] == null ? 'opacity: 0.5;' : '' }}">
      <div class="card-body text-center">
        <h5 class="card-title">November</h5>
        <hr>
        <p class="card-text">
        Total animal: {{ $reports['November']['total_animal']}} <br>
        Total dead: {{ $reports['November']['total_dead']}}
        </p>

        <a class="btn btn-primary" data-toggle="collapse" href="#November" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Summary</a>

            <div class="collapse multi-collapse" id="November">
              <div class="card card-body">
                {{ $reports['November']['summary']}}  
              </div>
            </div>

            @if ($reports['November'] == null)
            <a class="btn btn-primary" href="#">Edit</a>
            @else
            <a class="btn btn-primary" href="{{action('ReportController@edit',$reports['November']['id'])}}">Edit</a>
            @endif

      </div>
    </div>
    <div class="card bg-warning" style="{{ $reports['December'] == null ? 'opacity: 0.5;' : '' }}">
      <div class="card-body text-center">
        <h5 class="card-title">December</h5>
        <hr>
        <p class="card-text">
        Total animal: {{ $reports['December']['total_animal']}} <br>
        Total dead: {{ $reports['December']['total_dead']}}
        </p>

        <a class="btn btn-primary" data-toggle="collapse" href="#December" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Summary</a>

            <div class="collapse multi-collapse" id="December">
              <div class="card card-body">
                {{ $reports['December']['summary']}}  
              </div>
            </div>

            @if ($reports['December'] == null)
            <a class="btn btn-primary" href="#">Edit</a>
            @else
            <a class="btn btn-primary" href="{{action('ReportController@edit',$reports['December']['id'])}}">Edit</a>
            @endif

      </div>
    </div>  
</div>
{{-- <div class="card-deck">
    @foreach ($reports as $key => $report)
    @if ($loop->iteration == 3 || $loop->iteration == 6 || $loop->iteration == 9)
        
    <div class="card bg-warning">
      <div class="card-body text-center">
        <p class="card-text"> 
            {{ $key }} 
            <br>
            Total animal: {{ $report['total_animal'] }} <br>
            Total dead: {{ $report['total_dead'] }}
        </p>
      </div>
    </div> 
    
    @endif 
    @endforeach--}}  
</div>
<div class="col-md-2"></div>
</div>    

@endsection
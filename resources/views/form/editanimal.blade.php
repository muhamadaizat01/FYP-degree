@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-2 cs-padding-0">
            @include('layouts.horizontalnav')
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Animal Form</div>
            <div class="card-body">
                <h5 class="card-title">Edit Animal Entry Here</h5>
                
                @if(count($errors) > 0)
                <div class="alert alert danger">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form action="{{action('FormController@update', $id)}}" method="post" onsubmit="return confirm('Do you really want to update this animal details?');">
                {{ csrf_field() }}
                {{ method_field('PUT')}}
                <input type="hidden" name="_method" value="PUT"/>
                <div class="form-group">
                <label for="name">Name of Species:</label>
                <input type="text" class="form-control" id="name" name="name_of_species" placeholder="Elephant" value="{{$animal->name_of_species}}" readonly>
                </div>
                
                <div class="form-group">
                <label for="scientific_name">Scientific Name:</label>
                <input type="text" class="form-control" id="scientific_name" name="scientific_name" placeholder="Martis Kagiro" value="{{$animal->scientific_name}}" required>
                </div>

                <div class="form-group">
                <label for="unique_id">Unique ID:</label>
                <input type="text" class="form-control" id="unique_id" name="unique_id" placeholder="ABC123" value="{{$animal->unique_id}}" required>
                </div>

                <div class="form-group"> <!-- Date input -->
                    <label class="control-label" for="date">Date of Birth</label>
                    <input class="form-control" id="date" name="date_of_birth" type="text" value="{{$animal->date_of_birth}}" readonly/>
                </div>

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                           <label for="animal_category">Animal Category:</label>
                           <input class="form-control" id="animal_category" name="animal_category" type="text" value="{{$animal->animal_category}}" readonly/>
                        </div>
                        <div class="col-md-6">
                            <label for="animal_sex">Animal Sex:</label>
                            <input class="form-control" id="animal_sex" name="animal_sex" type="text" value="{{$animal->animal_sex}}" readonly/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-12">
                            <label for="habitat">Animal Habitat:</label>
                                <input type="text" class="form-control" id="animal_habitat" name="animal_habitat" list="suggestions" placeholder="e.g Lion Habitat" value="{{$animal->animal_habitat}}" readonly>
                            </div>
                        </div>
                    </div>

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6"> <!-- Date input -->
                            <label class="control-label" for="date">Date of Arrival</label>
                            <input class="form-control" id="date_of_arrival" name="date_of_arrival" type="date" value="{{$animal->date_of_arrival}}" required/>
                        </div>

                        <div class="col-md-6"> <!-- Time input -->
                            <label class="control-label" for="time">Time of Arrival</label>
                            <input class="form-control" id="time_of_arrival" name="time_of_arrival" type="time" value="{{$animal->time_of_arrival}}" required/>
                        </div>
                    </div>
                </div>    
                        <div class="form-group"> <!-- Date input -->
                            <label class="control-label" for="date">Date of Death</label>
                            <input class="form-control" id="date_of_death" name="date_of_death" type="date" value="{{$animal->date_of_death}}"/>
                        </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-info" value="Update">
                </div>
         

                    </div>
                </div>       
                    </div>
                    <div class="col-md-2"></div>
                </form>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
        if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        } 
    today = yyyy+'-'+mm+'-'+dd;
    document.getElementById("date").setAttribute("max", today);
    document.getElementById("date_of_death").setAttribute("max", today);
</script>

@endsection
{{-- <ul class="nav flex-column">
  <li class="nav-item">
    <a class="nav-link" href="{{ route('register') }}">Register New Staff</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/form/adminviewstaff">View Staff</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/form/adminaddanimal">Add Animal Entry</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/form/adminviewanimal">View Animal Entry</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/form/adminreport">Animal Monthly Summary Report</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/form/adminviewreport">View Animal Monthly Summary Report</a>
  </li>
</ul>
 --}}

<div class="sb-wrapper">
    <!-- Sidebar -->
    <nav id="sb-sidebar">
        <ul class="list-unstyled components">
            <p><strong>Menu</strong></p>
            <li class="{{ request()->segment(1) == 'register' ? 'active' : ''}}">
                <a href="{{ route('register') }}"  aria-expanded="false" >Register New Staff</a>
            </li>
            <li class="{{ request()->segment(2) == 'adminviewstaff' ? 'active' : ''}}">
                <a href="/form/adminviewstaff">View Staff List</a>
            </li>
            <li class="{{ request()->segment(2) == 'adminaddanimal' ? 'active' : ''}}">
                <a href="/form/adminaddanimal">Add Animal Entry</a>
            </li>
            <li class="{{ request()->segment(2) == 'adminviewanimal' ? 'active' : ''}}">
                <a href="/form/adminviewanimal">View Animal Entry</a>
            </li>
            <li class="{{ request()->segment(2) == 'adminreport' ? 'active' : ''}}">
                <a href="/form/adminreport">Animal Monthly<br>Summary Report</a>
            </li>
            <li class="{{ request()->segment(2) == 'adminviewreport' ? 'active' : ''}}">
                <a href="/form/adminviewreport">View Animal Monthly Summary Report</a>
            </li>
        </ul>
    </nav>

</div>
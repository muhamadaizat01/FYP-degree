{{-- <ul class="nav flex-column">
  <li class="nav-item">
    <a class="nav-link" href="/form/viewprofile/{{ auth()->user()->id }}">Profile</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/form/addanimal">Add Animal Entry</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/form/viewanimal">View Animal Entry</a>
  </li>
</ul> --}}

<div class="sd-wrapper">
    <!-- Sidebar -->
    <nav id="sd-sidebar">
        <ul class="list-unstyled components">
            <p><strong>Menu</strong></p>
            <li class="{{ request()->segment(2) == 'viewprofile' ? 'active' : ''}}">
                <a href="/form/viewprofile/{{ auth()->user()->id }}">Profile</a>
            </li>
            <li class="{{ request()->segment(2) == 'addanimal' ? 'active' : ''}}">
                <a href="/form/addanimal">Add Animal Entry</a>
            </li>
            <li class="{{ request()->segment(2) == 'viewanimal' ? 'active' : ''}}">
                <a href="/form/viewanimal">View Animal Entry</a>
            </li>
        </ul>
    </nav>

</div>